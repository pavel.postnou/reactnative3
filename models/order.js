const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const orderScheme = new Schema({
    number: {
        type:Number
    },
    products: [{
        type: mongoose.Schema.Types.ObjectId, ref: "Product",
        autopopulate: true
    }],
    cartId: {
        type: mongoose.Schema.Types.ObjectId, ref: "Cart"
    },
    user:{
        type: mongoose.Schema.Types.ObjectId, ref: "User"
    }
},
    { versionKey: false }
);  
orderScheme.plugin(require('mongoose-autopopulate'));
const Order = mongoose.model("Order", orderScheme);
module.exports = Order;
 