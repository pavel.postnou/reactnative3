const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const cartScheme = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId, ref: "User",
        
    },
    products: [{
        type: mongoose.Schema.Types.ObjectId, ref: "Product",
        autopopulate: true
    }]
},
    { versionKey: false }
);
cartScheme.plugin(require('mongoose-autopopulate'));
const Cart = mongoose.model("Cart", cartScheme);
module.exports = Cart;
