const express = require("express");
const orderController = require("../controllers/orderController.js");
const orderRout = express.Router();
const auth = require ("../auth.js");

orderRout.post("/create", auth.verifyToken, orderController.createOrder);
orderRout.delete("/:id", auth.verifyToken, orderController.deleteOrder);
orderRout.get("/:id", auth.verifyToken, orderController.getOrderById);
orderRout.get("/get/all", auth.verifyToken, orderController.getAllOrders);

module.exports = orderRout;
