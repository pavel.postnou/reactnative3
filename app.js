const mongoose = require("mongoose");
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const urlencodedParser = bodyParser.urlencoded({extended: true});


mongoose.connect("mongodb://localhost:27017/task10db",
{ useUnifiedTopology: true, 
    useNewUrlParser: true, 
    useFindAndModify: true })
    .then(() => {
        console.log("Successfully connected to database");
    })
    .catch((error) => {
        console.log("database connection failed. exiting now...");
        console.error(error);
        process.exit(1);
    });
    

    const authRouter = require('./routes/authRouter');
    const userRouter = require('./routes/userRouter');
    const cartRout = require('./routes/cartRout');
    const categoryRout = require('./routes/categoryRout');
    const homeRout = require('./routes/homeRout');
    const orderRout = require('./routes/orderRout');
    const productRout = require('./routes/productRout');
    app.use('', bodyParser.json(), authRouter);
    app.use('/user', urlencodedParser,urlencodedParser, userRouter);
    app.use('/cart', urlencodedParser, cartRout);
    app.use('/category', urlencodedParser, categoryRout);
    app.use('/home', urlencodedParser, homeRout);
    app.use('/order', urlencodedParser, orderRout);
    app.use('/product', urlencodedParser, productRout);


const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Trying to listen port ${PORT}.`);
})