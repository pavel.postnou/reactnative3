const User = require('../models/user')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const accessToken = require('../config');
const googleClientId = '858074835463-03l01lh3a6luc8817o7534n1d3kka1j4.apps.googleusercontent.com'
const googleSecret = "PuYPeUN1X0eztgmHpbk8Trtr";
const googleRedirect = 'http://localhost:3000/googleauth';
const {google} = require('googleapis');
const urlParse = require('url-parse');
const queryString = require('query-string');
const axios = require('axios');
const Cart = require('../models/cart.js');

const oauth2Client = new google.auth.OAuth2(
  googleClientId,
  googleSecret,
  googleRedirect);

const url = oauth2Client.generateAuthUrl({
  access_type: 'offline',
  scope: ['https://www.googleapis.com/auth/userinfo.profile',
          'https://www.googleapis.com/auth/userinfo.email'
  ]
});

const regUser = async (req,res) => {
  console.log(req.body)
    try {
        const {name, surname,email, password} = req.body;
    if (!(email && password && surname && name)) {
      res.status(400).send("All input is required");
    }
    const oldUser = await User.findOne({ email });
    if (oldUser) {
      return res.status(409).send("User Already Exist. Please email");
    }
    role = req.body.role;
    encryptedPassword = await bcrypt.hash(password, 10);
    const user = await User.create({
      name,
      surname,
      email: email.toLowerCase(), 
      password: encryptedPassword,
      role: role,
    });
    const token = jwt.sign(
      { user_id: user._id, email },
        accessToken,
      {
        expiresIn: "2h",
      }
    );
    if (user.cart) {
        response.status(400).send("У вас уже есть одна корзина")
    }
    else {
        const newCart = await new Cart({});
        newCart.user = user._id;
        await newCart.save();
        user.cart = newCart._id;
        await user.save();
    }
    user.token = token;
    res.status(201).json(user);
  } catch (err) {
    console.log(err);
  }
};


const loginUser = async (req, res) => {
  try {
    const { email, password } = req.body;
    if (!(email && password)) {
      res.status(400).send("All input is required");
    }
    const user = await User.findOne({ email });
    if (user && (await bcrypt.compare(password, user.password))) {
      const token = jwt.sign(
        { user_id: user._id, email },
          accessToken,
        {
          expiresIn: "2h",
        }
      );
      user.token = token;
      res.status(200).json(user);
    }
    res.status(401).send("Invalid Credentials");
  } catch (err) {
    console.log(err);
  }
};

const googleLogin = async (req,res) => {
  res.send(url)
}

const googleAuth = async (req,res) => {
  try{
    const googleUrl = new urlParse(req.url);
    const code = queryString.parse(googleUrl.query).code;
    const {tokens} = await oauth2Client.getToken(code);

    const {data} = await axios({
      url: 'https://www.googleapis.com/oauth2/v2/userinfo',
      method: 'get',
      headers: {
        authorization: `Bearer ${tokens.access_token}`
      }
    })

    const user = await User.findOne({email: data.email});
    if(!user){
      const createUser = new User({name: data.given_name, surname: data.family_name, email: data.email});
      await createUser.save();
      const accessTokenForUser = jwt.sign({name: createUser.name, role: createUser.role, _id: createUser._id}, accessToken, {expiresIn: '25h'});
      return res.send(`${createUser} ${accessTokenForUser}`)
    } else {
      res.status(400).send('user error')
    }
  }catch (err){
    res.status(400).send('Some error')
  }
}

module.exports = { regUser, loginUser, googleLogin, googleAuth };