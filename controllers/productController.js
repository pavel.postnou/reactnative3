const Category = require('../models/category.js');
const Product = require('../models/product.js');

exports.createProduct = async function (request, response) {
    try {
        let product = await Product.findOne({ name: request.body.name });

        let category = await Category.findOne({ name: request.body.category });

        if (!category) {
            response.send("Такой категории не существует")
        }

       else if (product) {
            response.status(400).send("Такой товар уже существует")
        }
        
        else
        {
            const newProduct = await new Product({ name: request.body.name});
            newProduct.category.push(category._id)
            await newProduct.save();
            category.products.push(newProduct._id);
            await category.save();
            response.send(`Новый товар создан: ${newProduct.name}`);
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}

exports.deleteProduct = async function (request, response) {
    try {
        let product = await Product.findOne({ _id: request.params.id });
        let category = await Category.findOne({ _id: product.category });
        if (product) {
            Product.deleteOne({ _id: request.params.id }, function (err, result) {
                if (err) return console.log(err);
            });
            for (let i = 0; i < category.products.length; i++) {
                if (category.products[i]._id.toString() == product._id.toString()) {
                    category.products.splice(category.products.indexOf(i), 1);
                    await category.save()
                }
            }
            response.send(`Удалён товар ${product.name}`);
        }
        else {
            response.send("Такого товара нет");
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло нет так")
    }
}

exports.updateProduct = async function (request, response) {
    try {
        let product = await Product.findOne({ _id: request.params.id });
        if (product) {
            Product.updateOne({ _id: request.params.id }, { name: request.body.name }, function (err, result) {
                if (err) return console.log(err);
            });
            response.send(`Товар изменён ${product.name}`);
        }
        else {
            response.send("Такого товара нет");
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}

exports.getProductById = async function (request, response) {
    try {
        let product = await Product.findOne({ _id: request.params.id });
        if (product) {
            response.send(`Товар: ${product.name}`)
        }
        else {
            response.send("Такого товара нет")
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}

exports.getAllProducts = async function (request, response) {
    try {
        let products = await Product.find();
        if (products) { 
            response.status(200).json({products})
        }
        else {
            response.status(401).send("Товары отсутствуют")
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}
