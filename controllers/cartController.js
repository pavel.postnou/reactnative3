const Cart = require('../models/cart.js');
const User = require('../models/user.js');
const Product = require('../models/product.js');

exports.createCart = async function (request, response) {
    try {
        let user = await User.findOne({ _id: request.decoded.user_id });
        if (user.cart) {
            response.status(400).send("У вас уже есть одна корзина")
        }
        else {
            const newCart = await new Cart({});
            newCart.user = request.decoded.user_id;
            await newCart.save();
            user.cart = newCart._id;
            await user.save();
            response.send(newCart);
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}

exports.getCartById = async function (request, response) {
    
    try {
        let cart = await Cart.findOne({ _id: request.params.id });
       
        if (cart) {
            response.status(200).json({cart})
        }
        else {
            const newCart = await new Cart({});
            newCart.user = request.decoded.user_id;
            await newCart.save();
            user.cart = newCart._id;
            await user.save();
            response.status(200).json({newCart});
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}

exports.addProductToCart = async function (request, response) {
    try {
        let cartUser = await User.findOne({ _id: request.decoded.user_id });
        let cart = await Cart.findOne({ _id: cartUser.cart._id });
        let product = await Product.findOne({ name: request.body.name });

        if (!cart) {
            response.send("У вас нет корзины");
        } else if (!product) {
            response.send("Такого товара не существует")
        }
        else if (product && cart) {
            console.log("complete")
            cart.products.push(product._id.toString());
            await cart.save();
            response.status(200).json({product})
        }
        else {
            response.send("Такой корзины нет")
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}
