import {SHOW_USER} from '../constants/user.constants'
export const defaultState = {
    prodLength:0
}

export const ReducerFunction =  (state = defaultState, action) => {
    switch (action.type) {
        case SHOW_USER:
            let newState = {
                ...state,...action.data
            }
            return newState;
        default:
            return state;
    }
};
