import React, {useReducer} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Context from "./utils/context";
import * as ACTIONS from "./actionCreators/users.action";
import { ReducerFunction, defaultState } from "./reducers/user.reducer";
import { MainStackNavigator } from "./navigation/StackNavigator";

export default function App() {

  const [stateUser, dispatchUserReducer] = useReducer(ReducerFunction, defaultState);
  const handleShowUser = (data) => {
    dispatchUserReducer(ACTIONS.showUser(data));
  };

  return (
    <Context.Provider value={{
      userState: stateUser,
      handleShowUser: (data) => handleShowUser(data)
    }} >
      <NavigationContainer>
      <MainStackNavigator/>
      </NavigationContainer>
    </Context.Provider>
  );
}