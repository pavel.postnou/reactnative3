import {StyleSheet, Text, View, ImageBackground } from 'react-native';
import React from 'react';


export default function DrawOne() {
    const img = { uri: "https://png.pngtree.com/thumb_back/fw800/background/20190703/pngtree-vector-white-background-with-abstract-geometric-pattern-of-3d-tr-image_282961.jpg" }
    return (
        <View style={styles.container}>
          <ImageBackground source={img} resizeMode="cover" style={styles.image}>
            <Text style={{ ...styles.titleText }}>Статическая информация 1</Text>
          </ImageBackground>
        </View>
      );
}

const styles = StyleSheet.create({

    image: {
      width: "100%",
      flex: 1,
    },
    titleText: {
      color: "darkblue",
      fontSize: 20
    },
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center"
    },
  });