import { StyleSheet, ScrollView, Text, View, ImageBackground} from 'react-native';
import React, { useState, useEffect, useContext } from 'react';
import Context from "../utils/context";
import { orderGet } from '../services/orders';
let id = 1;

export default function Order({route}) {
  const context = useContext(Context)
  const [order, setOrder] = useState([])

  useEffect(() => {
    (async () => {
      const orders = await orderGet(context.userState.token);
      setOrder(orders);
    })()
  }, [route.params])

  const img = { uri: "https://png.pngtree.com/thumb_back/fw800/background/20190703/pngtree-vector-white-background-with-abstract-geometric-pattern-of-3d-tr-image_282961.jpg" }
  return (
    <View style={styles.container}>
      <ImageBackground source={img} resizeMode="cover" style={styles.image}>
        <ScrollView style={styles.scrollView}>
          <Text style={styles.titleText}>Ваши заказы</Text>
          {order.length > 0 ? order.map(item => (<View key={item._id} style={styles.container}>
            <Text style={styles.textStyle}>Номер : {item.number}</Text>
            {item.products.length > 0 ? item.products.map(products => (<Text key={id++} style={styles.titleText}>{products.name}</Text>)):null}
          </View>)) : null}
        </ScrollView>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({

  image: {
    width: "100%",
    flex: 1,
    alignItems:"center"
  },
  scrollView: {
    marginHorizontal: 20
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    marginTop: 20,
    elevation: 2
  },
  titleText: {
    color: "darkblue",
    fontSize: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop:10,
    marginBottom:10
  },
  textStyle: {
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop:10,
    marginBottom:10
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor:"lightgrey",
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginBottom:20
  },
});