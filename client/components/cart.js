import { StyleSheet, Modal, ScrollView, TouchableHighlight, Text, View, ImageBackground, Button } from 'react-native';
import React, { useState, useEffect, useContext } from 'react';
import { getUserProducts } from "../services/userProducts"
import Context from "../utils/context";
import { orderCreate } from '../services/orders';
import { scheduleRemoveNotification } from '../services/notification';
let id = 1;

export default function Cart({ route }) {
  const context = useContext(Context)
  const [products, setProducts] = useState([])
  const [modalVisible, setModalVisible] = useState(false);
  const [prodLength,setProdlength] = useState(0);
  useEffect(() => {
    (async () => {
      const productsArr = await getUserProducts(context.userState.cartId, context.userState.token);
      console.log(productsArr.length)
      setProdlength(productsArr.length)
      context.handleShowUser({prodLength})
      setProducts(productsArr);
    })()
  }, [route.params])

  async function createOrder() {
    orderCreate(context.userState.token)
    context.userState.notification = false
    scheduleRemoveNotification()
  }

  const img = { uri: "https://png.pngtree.com/thumb_back/fw800/background/20190703/pngtree-vector-white-background-with-abstract-geometric-pattern-of-3d-tr-image_282961.jpg" }
  return (
    <View style={styles.container}>
      <ImageBackground source={img} resizeMode="cover" style={styles.image}>
        <ScrollView style={styles.scrollView}>
          <Text style={styles.titleText}>Продукты в корзине</Text>
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <Text style={styles.modalText}>Вы уверены?</Text>
                <TouchableHighlight
                  style={{ ...styles.openButton, backgroundColor: '#2196F3' }}
                  onPress={() => {
                    createOrder(), setModalVisible(!modalVisible);
                  }}>
                  <Text style={styles.textStyle}>Да</Text>
                </TouchableHighlight>
                <TouchableHighlight
                  style={{ ...styles.openButton, backgroundColor: 'red' }}
                  onPress={() => {
                    setModalVisible(!modalVisible);
                  }}>
                  <Text style={styles.textStyle}>Нет</Text>
                </TouchableHighlight>
              </View>
            </View>
          </Modal>
          {products.length > 0 ? products.map(item => (<View key={id++} style={styles.container}>
            <Text style={styles.textStyle}>Продукт : {item.name}</Text>
          </View>)) : null}
          <Button onPress={() => {
            setModalVisible(!modalVisible);
          }} title="Оформить заказ"></Button>
        </ScrollView>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({

  image: {
    width: "100%",
    flex: 1,
    alignItems: "center"
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  modalView: {
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 10,
    alignItems: "stretch",
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    color: "red"
  },
  scrollView: {
    marginHorizontal: 20
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 10,
    padding: 10,
    marginTop: 30,
    elevation: 2
  },
  titleText: {
    color: "darkblue",
    fontSize: 20,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "lightgrey",
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginBottom: 20
  },
});