import { StyleSheet, ScrollView, Text, View, ImageBackground, Button } from 'react-native';
import React, { useState, useEffect, useContext } from 'react';
import { getAllProducts } from "../services/products"
import Context from "../utils/context";
import { addProductToCart } from '../services/products';
import {schedulePushNotification} from '../services/notification';

export default function Products() {
  const context = useContext(Context)
  const [products, setProducts] = useState([])
  
  useEffect(() => {
    (async () => {
      const productsArr = await getAllProducts(context.userState.token);
      setProducts(productsArr);
    })()
  },)
 
  async function addProduct(name) {
    addProductToCart({name}, context.userState.token)
    const prodLength = context.userState.prodLength + 1
    context.handleShowUser({prodLength})
    if (context.userState.notification !== true) {
      context.userState.notification = true
      schedulePushNotification()
    }
  }

  const img = { uri: "https://png.pngtree.com/thumb_back/fw800/background/20190703/pngtree-vector-white-background-with-abstract-geometric-pattern-of-3d-tr-image_282961.jpg" }
  return (
    <View style={styles.container}>
      <ImageBackground source={img} resizeMode="cover" style={styles.image}>
        <ScrollView style={styles.scrollView}>
          <Text style={styles.titleText}>Продукты</Text>
          {products.length > 0 ? products.map(item => (<View key={item._id} style={styles.container}>
            <Text style={styles.textStyle}>Продукт : {item.name}</Text>
            <Text style={styles.textStyle}>Категория : {item.category[0].name}</Text>
            <Button onPress={() => addProduct(item.name)} title="добавить в корзину"></Button>
          </View>)) : null}
        </ScrollView>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({

  image: {
    width: "100%",
    flex: 1,
    alignItems: "center"
  },
  scrollView: {
    marginHorizontal: 20
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    marginTop: 20,
    elevation: 2
  },
  titleText: {
    color: "darkblue",
    fontSize: 20,
    alignItems: "center",
    justifyContent: "center"
  },
  textStyle: {
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "lightgrey",
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: 20
  },
});