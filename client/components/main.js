import { StyleSheet, Text, View, ImageBackground } from 'react-native';
import React, { useContext, useEffect, useState } from 'react';
import Context from '../utils/context';
import { getUserProducts } from '../services/userProducts';
import {schedulePushNotification} from '../services/notification';

export default function MainScreen() {
  
  const img = { uri: "https://png.pngtree.com/thumb_back/fw800/background/20190703/pngtree-vector-white-background-with-abstract-geometric-pattern-of-3d-tr-image_282961.jpg" }
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");
  const context = useContext(Context)
  useEffect(() => {
    setName(context.userState.name),
      setSurname(context.userState.surname)
  })

  useEffect(() => {
    (async () => {
      const products = await getUserProducts(context.userState.cartId, context.userState.token);
      if (products.length > 0)
      {
        schedulePushNotification()
        context.userState.notification = true
      }
    })()
  },)

  return (
    <View >
      <ImageBackground source={img} resizeMode="cover" style={styles.image}>
        <View style={styles.infoView}>
        <Text style={{ ...styles.titleText }}>Вы вошли как пользователь</Text>
        <Text style={{ ...styles.titleText }}>Имя: {name}</Text>
        <Text style={{ ...styles.titleText }}>Фамилия: {surname}</Text>
        </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({

  image: {
    marginTop:100,
    width: "100%",
    alignItems:"center",
    flex: 1,
  },
  infoView: {
    height:100,
    margin: 10,
    backgroundColor: 'lightblue',
    borderRadius: 10,
    padding: 10,
    alignItems: "center"
  },
  titleText: {
    color: "black",
    fontSize: 20
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
});