import React, {useEffect, useContext, useState} from "react";
import ProductScreen from '../components/products';
import ProfileScreen from '../components/main';
import CartScreen from '../components/cart';
import OrderScreen from '../components/orders';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Context from "../utils/context";

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {

  const context = useContext(Context)
  const [badge, setBadge] = useState('');

  useEffect(() =>{
    setBadge(context.userState.prodLength)
  },[context.userState.prodLength])

  return (
    <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ color, size }) => {
        let iconName;
        if (route.name === 'Profile') {
          iconName = 'body';
        } else if (route.name === 'Products') {
          iconName ='clipboard';
        }
        else if (route.name === 'Cart') {
          iconName ='cart';
        }
        else if (route.name === 'Orders') {
          iconName ="receipt";
        }
        return <Ionicons name={iconName} size={size} color={color} />;
      },
      tabBarActiveTintColor: 'red',
      tabBarInactiveTintColor: 'gray',
    })}>
      <Tab.Screen name="Profile" component={ProfileScreen}  options={{ headerShown: false }} />
      <Tab.Screen name="Products" component={ProductScreen} options={{ headerShown: false }}/>
      <Tab.Screen name="Cart" initialParams={{param:true}} component={CartScreen} options={{ headerShown: false, tabBarBadge: badge }} />
      <Tab.Screen name="Orders" initialParams={{param:true}} component = {OrderScreen} options={{ headerShown: false, tabBarBadge: badge }}  />
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;