import * as Notifications from 'expo-notifications';

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});


export async function schedulePushNotification() {
 
  await Notifications.scheduleNotificationAsync({
    content: {
      title: "Напоминание 📣",
      body: 'У вас есть неоформленный заказ'
    },
    trigger: { seconds: 1 },
  });
}

export async function scheduleRemoveNotification() {
    Notifications.dismissNotificationAsync()
}
