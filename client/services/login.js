
import { Alert } from "react-native";
import { loginSchema } from "../utils/login";

const URL = 'http://192.168.1.112:3000'

export async function logIn(data) {
    try {
    // await loginSchema.validate(data)
    const result = await fetch(`${URL}/login`,{
        method:'POST',
        body: JSON.stringify(data),
        headers:{
            'Content-Type': 'application/json'
        }
    })
    
    if (result.status == 200) {
    const json = await result.json()
        return (json) 
    } 
    // else if (result.status == 401)
    // {
    //     Alert.alert ('Ошибка','Введён неверный email или пароль');
    //     return false;
    // }
    // else if (result.status == 400)
    // {
    //     Alert.alert ('Ошибка','Не все поля заполнены');
    //     return false;
    // }
    // else {
    //     Alert.alert ('Ошибка','Что то пошло не так');
    //     return false;
    // }
}
    catch(e) {
        Alert.alert("Error",`${e.message}`)
    }
}