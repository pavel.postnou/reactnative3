
import { Alert } from "react-native";

const URL = 'http://192.168.1.112:3000'

export async function getUserProducts(cartId, token) {

    const result = await fetch(`${URL}/cart/${cartId}`,{
        method:'GET',
        headers:{
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + token
        }
    })
    if (result.status == 200) {
    const json = await result.json()
        return json.cart.products 
    } 
    else {
        Alert.alert ('Ошибка','Что то пошло не так');
        return false;
    }
}

