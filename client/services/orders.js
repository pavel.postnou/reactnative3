
import { Alert } from "react-native";

const URL = 'http://192.168.1.112:3000'

export async function orderCreate(token) {
    const result = await fetch(`${URL}/order/create`,{
        method:'POST',
        headers:{
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + token
        }
    })
    if (result) {
        Alert.alert("Заказ сформирован")

    } 
    else if (result.status == 401)
    {
        Alert.alert ('Ошибка','Товары отсутствуют');
        return false;
    }
    else {
        Alert.alert ('Ошибка','Что то пошло не так');
        return false;
    }
}

export async function orderGet(token) {
    const result = await fetch(`${URL}/order/get/all`,{
        method:'GET',
        headers:{
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + token
        }
    })
    if (result) {
        const res = await result.json()
        return res.order
    } 
}


