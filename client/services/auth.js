import { Alert } from "react-native";
import { loginSchema } from "../utils/login";

const URL = 'http://192.168.1.112:3000'

export async function registration(data) {
    try {
        await loginSchema.validate(data)
    const result = await fetch(`${URL}/register`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    if (result.status == 201) {
        const json = await result.json()
        return (json) 
    }
    // else if (result.status == 400) {
    //     Alert.alert("Ошибка", "Не все поля заполнены")
    //     return false;
    // }
    // else if (result.status == 409) {
    //     Alert.alert("Ошибка", "Пользователь с таким email уже существует")
    //     return false;
    // }

    Alert.alert('Ошибка', 'Что то пошло не так');
    return false;
}
catch (e) {
    Alert.alert("Error",`${e.message}`)
}
}